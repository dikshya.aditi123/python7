-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2023 at 06:23 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `artandcollectibles`
--

-- --------------------------------------------------------

--
-- Table structure for table `art_admin`
--

CREATE TABLE `art_admin` (
  `id` bigint(20) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `art_admin`
--

INSERT INTO `art_admin` (`id`, `full_name`, `image`, `mobile`, `user_id`) VALUES
(1, 'Yubrajkhadka', 'admins/1.jpg', '9861562381', 46),
(3, 'ads m', 'admins/wall5.jpg', '9879103939', 60);

-- --------------------------------------------------------

--
-- Table structure for table `art_category`
--

CREATE TABLE `art_category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `art_category`
--

INSERT INTO `art_category` (`id`, `name`, `slug`) VALUES
(2, 'Drawing', 'drawing'),
(3, 'Sketch', 'sketch'),
(4, 'Painting', 'painting'),
(21, 'Mandala', 'mandala');

-- --------------------------------------------------------

--
-- Table structure for table `art_contact`
--

CREATE TABLE `art_contact` (
  `id` bigint(20) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` longtext NOT NULL,
  `number` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `art_contact`
--

INSERT INTO `art_contact` (`id`, `name`, `email`, `subject`, `message`, `number`) VALUES
(9, 'jasmine neupane', 'jasmineneupane@kcc.edu.np', 'nice', 'good', 'False'),
(10, 'aditi maskey', 'aditimaskey@gmail.com', 'yes', 'no', 'False'),
(12, 'yubraj', 'melamchi@gmail.com', 'project', 'nice', 'False'),
(19, 'yubraj', 'aditi@gmail.com', 'good', 'nice', 'False'),
(20, 'uu4ufu', 'jkrjjrj@gmail.com', 'Good', 'this is good', 'False'),
(21, 'yubraj khadka', 'khadkayubraj239@gmail.com', 'product', 'good product', 'False'),
(22, 'aditi maskey', 'aditi@gmail.com', 'nice', 'goood', 'False'),
(23, 'aditi maskey', 'aditi@gmail.com', 'good', 'nicw', '9808482819'),
(24, 'yubraj khadka', 'khadkayubraj239@gmail.com', 'good', 'it is nice', '9861562381'),
(25, 'yubraj khadka', 'khadkayubraj239@gmail.com', 'good', 'it is nice', '9861562381'),
(26, 'aditi maskey', 'maskey@gmail.com', 'nice', 'it is nice', '9861562381'),
(27, 'yubraj khadka', 'khadkayubraj239@gmail.com', 'yyyyy', 'yyyyy', '9861562381'),
(28, 'yubraj khadka', 'khadkayubraj239@gmail.com', 'BBB', 'BB  BB B ', '9861562381'),
(29, 'PRAJITA khatri', 'khadka@gmail.com', 'yubraj', 'melamchi', '9861562381'),
(30, 'ads m', 'ads@gmail.com', 'hi', 'hi like what youre doing', '9879103939');

-- --------------------------------------------------------

--
-- Table structure for table `art_order`
--

CREATE TABLE `art_order` (
  `id` bigint(20) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` longtext NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(60) NOT NULL,
  `additional_info` longtext NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `date` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `paid` tinyint(1) DEFAULT NULL,
  `payment_id` varchar(200) DEFAULT NULL,
  `order_status` varchar(50) NOT NULL,
  `payment_method` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `art_order`
--

INSERT INTO `art_order` (`id`, `firstname`, `lastname`, `address`, `phone`, `email`, `additional_info`, `amount`, `date`, `user_id`, `paid`, `payment_id`, `order_status`, `payment_method`) VALUES
(181, 'aditi', 'maskey', 'bhaktapur', '9803045898', 'aditi@gmail.com', 'good', '6600.00', '2023-02-25 16:16:19.117259', 45, 0, NULL, 'On the way', 'Cash On Delivery'),
(182, 'aditi', 'maskey', 'bhaktapur', '9803045898', 'aditi@gmail.com', 'good', '6600.00', '2023-02-25 16:16:52.911362', 45, 1, NULL, 'Order Processing', 'Khalti'),
(194, 'eee', 'ggg', 'melamchi', '9803045898', 'yubraj@gmail.com', 'dddd', '900.00', '2023-03-14 12:59:00.394204', 1, 0, NULL, 'Order Completed', 'Cash On Delivery'),
(203, 'nabaraj', 'khadka', 'melamchi', '9808482819', 'khadkayubraj239@gmail.com', 'good', '600.00', '2023-03-15 04:35:58.675028', 1, 0, NULL, 'On the way', 'Cash On Delivery'),
(204, 'aditi', 'maskey', 'melamchi', '9840000', 'aditi@gmail.com', '9861562381', '600.00', '2023-03-15 04:45:11.175538', 45, 0, NULL, 'Order Completed', 'Cash On Delivery'),
(215, 'Yubraj', 'Khadka', 'melamchi', '9861562381', 'khadkayubraj239@gmail.com', 'good', '800.00', '2023-03-17 11:29:40.211059', 46, 0, NULL, 'Order Cancelled', 'Cash On Delivery'),
(247, 'eee', 'ggg', 'melamchi', '9861562381', 'yubraj@gmail.com', 'good', '1300.00', '2023-03-21 12:15:50.802614', 46, 0, NULL, 'Order Received', 'Khalti'),
(261, 'ads', 'm', 'bkt', '9861562381', 'ads@gmail.com', 'lol', '600.00', '2023-03-24 16:00:26.660217', 60, 1, NULL, 'Order Received', 'Khalti');

-- --------------------------------------------------------

--
-- Table structure for table `art_orderitem`
--

CREATE TABLE `art_orderitem` (
  `id` bigint(20) NOT NULL,
  `product` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `quantity` varchar(20) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `order_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `art_orderitem`
--

INSERT INTO `art_orderitem` (`id`, `product`, `image`, `quantity`, `price`, `total`, `order_id`) VALUES
(181, 'Drawing1', '/media/photos/products/draw1.jpg', '1', '400.00', '400.00', 181),
(182, 'Mandala1', '/media/photos/products/bmandala1.jpeg', '1', '6000.00', '6000.00', 181),
(183, 'Drawing1', '/media/photos/products/draw1.jpg', '1', '400.00', '400.00', 182),
(184, 'Mandala1', '/media/photos/products/bmandala1.jpeg', '1', '6000.00', '6000.00', 182),
(196, 'Sketch1', '/media/photos/products/s3.jpg', '1', '700.00', '700.00', 194),
(202, 'Drawing2', '/media/photos/products/drawing1.jpg', '1', '400.00', '400.00', 203),
(203, 'Drawing2', 'order/photos/3.JPG', '1', '400.00', '400.00', 204),
(220, 'Drawing4', '/media/photos/products/drawings.jpg', '1', '600.00', '600.00', 215),
(253, 'Sketch1', '/media/photos/products/s3.jpg', '1', '700.00', '700.00', 247),
(254, 'Drawing2', '/media/photos/products/drawing1.jpg', '1', '400.00', '400.00', 247),
(270, 'Drawing2', '/media/photos/products/drawing1.jpg', '1', '400.00', '400.00', 261);

-- --------------------------------------------------------

--
-- Table structure for table `art_product`
--

CREATE TABLE `art_product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `image` varchar(100) NOT NULL,
  `stock` int(11) NOT NULL,
  `is_available` tinyint(1) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `modified_date` datetime(6) NOT NULL,
  `quantity` int(11) NOT NULL,
  `category_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `art_product`
--

INSERT INTO `art_product` (`id`, `name`, `slug`, `description`, `price`, `image`, `stock`, `is_available`, `created_date`, `modified_date`, `quantity`, `category_id`) VALUES
(3, 'Sketch1', 'sketch1', 'This is the sketch', '700.00', 'photos/products/s3.jpg', 1, 1, '2023-02-08 09:58:15.713116', '2023-03-20 05:52:18.332506', 1, 3),
(4, 'Painting1', 'painting1', 'This is the painting', '7000.00', 'photos/products/apaint3.jpg', 0, 1, '2023-02-08 09:58:54.965986', '2023-03-19 14:25:31.026371', 1, 4),
(6, 'Drawing2', 'drawing2', 'This is the beautiful Drawing', '400.00', 'photos/products/drawing1.jpg', 3, 1, '2023-02-08 10:00:19.645345', '2023-02-08 10:00:19.645345', 1, 2),
(8, 'Drawing3', 'drawing3', 'This is the good drawing', '500.00', 'photos/products/drawing4.jpg', 8, 1, '2023-02-16 10:16:17.210627', '2023-03-17 09:10:25.591614', 5, 2),
(9, 'Drawing4', 'drawing4', 'This is funny drawing', '600.00', 'photos/products/drawings.jpg', 2, 1, '2023-02-16 10:17:07.917146', '2023-02-16 10:17:07.917146', 1, 2),
(10, 'Drawing7', 'drawing7', 'This is the nice drawing', '800.00', 'photos/products/drawing3.jpg', 3, 1, '2023-02-16 10:18:07.172395', '2023-02-16 10:18:07.172395', 1, 2),
(13, 'Drawing8', 'drawing8', 'This is the drawing', '400.00', 'photos/products/draw3.jpg', 5, 1, '2023-03-17 08:24:27.716723', '2023-03-17 08:24:27.716723', 1, 2),
(15, 'Mandala1', 'mandala1', 'This is the good mandala', '400.00', 'photos/products/bmandala1_hRmcKLd.jpeg', 3, 1, '2023-03-17 16:20:52.574771', '2023-03-17 16:20:52.574771', 1, 21),
(16, 'Mandala2', 'mandala2', 'This is the good mandala', '300.00', 'photos/products/bmandala2.jpeg', 3, 1, '2023-03-17 16:21:38.176944', '2023-03-17 16:35:54.684924', 1, 21),
(17, 'Mandala3', 'mandala3', 'This is the mandala3', '8000.00', 'photos/products/bmandala3_cOyWn0a.jpeg', 3, 1, '2023-03-17 16:22:24.113281', '2023-03-17 16:22:24.113281', 1, 21),
(18, 'Sketch2', 'sketch2', 'This is the good sketch', '10000.00', 'photos/products/s2.jpeg', 2, 1, '2023-03-17 16:24:12.224184', '2023-03-17 16:24:12.224184', 1, 3),
(19, 'Sketch3', 'sketch3', 'This is the good sketch', '3000.00', 'photos/products/sketches.jpg', 4, 1, '2023-03-17 16:24:52.042867', '2023-03-20 04:47:02.847694', 1, 3),
(20, 'Painting2', 'painting2', 'This is the painting', '200.00', 'photos/products/apaint2.jpg', 6, 1, '2023-03-17 16:26:04.407017', '2023-03-17 16:33:03.603323', 1, 4),
(21, 'Painting3', 'painting3', 'This is the good painting', '6000.00', 'photos/products/apaint1_m9b2YzY.jpg', 5, 1, '2023-03-17 16:26:47.118253', '2023-03-17 16:26:47.118253', 1, 4),
(22, 'drawing79', 'drawing79', 'This is the drawing', '500.00', 'photos/products/draw2.jpg', 6, 1, '2023-03-20 08:13:06.270675', '2023-03-20 08:13:35.642619', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `art_wishlist`
--

CREATE TABLE `art_wishlist` (
  `id` bigint(20) NOT NULL,
  `added_date` datetime(6) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `art_wishlist`
--

INSERT INTO `art_wishlist` (`id`, `added_date`, `product_id`, `user_id`) VALUES
(15, '2023-02-22 15:34:49.722570', 3, 1),
(19, '2023-02-24 01:41:18.438902', 9, 47),
(21, '2023-03-19 12:12:07.913463', 9, 46),
(22, '2023-03-20 07:32:37.175219', 4, 49),
(23, '2023-03-20 07:50:44.127709', 4, 1),
(24, '2023-03-20 14:58:57.764625', 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add customer', 7, 'add_customer'),
(26, 'Can change customer', 7, 'change_customer'),
(27, 'Can delete customer', 7, 'delete_customer'),
(28, 'Can view customer', 7, 'view_customer'),
(29, 'Can add customer order', 8, 'add_customerorder'),
(30, 'Can change customer order', 8, 'change_customerorder'),
(31, 'Can delete customer order', 8, 'delete_customerorder'),
(32, 'Can view customer order', 8, 'view_customerorder'),
(33, 'Can add category', 9, 'add_category'),
(34, 'Can change category', 9, 'change_category'),
(35, 'Can delete category', 9, 'delete_category'),
(36, 'Can view category', 9, 'view_category'),
(37, 'Can add product', 10, 'add_product'),
(38, 'Can change product', 10, 'change_product'),
(39, 'Can delete product', 10, 'delete_product'),
(40, 'Can view product', 10, 'view_product'),
(41, 'Can add product image', 12, 'add_productimage'),
(42, 'Can change product image', 12, 'change_productimage'),
(43, 'Can delete product image', 12, 'delete_productimage'),
(44, 'Can view product image', 12, 'view_productimage'),
(45, 'Can add category', 11, 'add_category'),
(46, 'Can change category', 11, 'change_category'),
(47, 'Can delete category', 11, 'delete_category'),
(48, 'Can view category', 11, 'view_category'),
(49, 'Can add product', 13, 'add_product'),
(50, 'Can change product', 13, 'change_product'),
(51, 'Can delete product', 13, 'delete_product'),
(52, 'Can view product', 13, 'view_product'),
(53, 'Can add wishlist', 14, 'add_wishlist'),
(54, 'Can change wishlist', 14, 'change_wishlist'),
(55, 'Can delete wishlist', 14, 'delete_wishlist'),
(56, 'Can view wishlist', 14, 'view_wishlist'),
(57, 'Can add cart', 15, 'add_cart'),
(58, 'Can change cart', 15, 'change_cart'),
(59, 'Can delete cart', 15, 'delete_cart'),
(60, 'Can view cart', 15, 'view_cart'),
(61, 'Can add contact', 16, 'add_contact'),
(62, 'Can change contact', 16, 'change_contact'),
(63, 'Can delete contact', 16, 'delete_contact'),
(64, 'Can view contact', 16, 'view_contact'),
(65, 'Can add customer', 17, 'add_customer'),
(66, 'Can change customer', 17, 'change_customer'),
(67, 'Can delete customer', 17, 'delete_customer'),
(68, 'Can view customer', 17, 'view_customer'),
(69, 'Can add order item', 18, 'add_orderitem'),
(70, 'Can change order item', 18, 'change_orderitem'),
(71, 'Can delete order item', 18, 'delete_orderitem'),
(72, 'Can view order item', 18, 'view_orderitem'),
(73, 'Can add order', 19, 'add_order'),
(74, 'Can change order', 19, 'change_order'),
(75, 'Can delete order', 19, 'delete_order'),
(76, 'Can view order', 19, 'view_order'),
(77, 'Can add captcha store', 20, 'add_captchastore'),
(78, 'Can change captcha store', 20, 'change_captchastore'),
(79, 'Can delete captcha store', 20, 'delete_captchastore'),
(80, 'Can view captcha store', 20, 'view_captchastore'),
(81, 'Can add admin', 21, 'add_admin'),
(82, 'Can change admin', 21, 'change_admin'),
(83, 'Can delete admin', 21, 'delete_admin'),
(84, 'Can view admin', 21, 'view_admin'),
(85, 'Can add rating', 22, 'add_rating'),
(86, 'Can change rating', 22, 'change_rating'),
(87, 'Can delete rating', 22, 'delete_rating'),
(88, 'Can view rating', 22, 'view_rating');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$390000$HI4LNxNCwIaV5dtIpDvpmi$K6l/FCDHr9wfcgm39Q9ctEPoElewmXxKEFShvJ+04R4=', '2023-03-24 16:02:24.674019', 1, 'admin', '', '', '', 1, 1, '2023-02-08 05:59:34.111475'),
(44, 'pbkdf2_sha256$390000$Us8FjI4pwKcdlLvU8OHXLN$uUVAoyisq7U6Gl3CGMAgf0uB4YfP5wqpytU8lVTGKxk=', '2023-02-20 15:38:59.374155', 0, 'aditi@', 'Aditi', 'Maskey', 'aditi@gmail.com', 0, 1, '2023-02-20 15:38:48.653371'),
(45, 'pbkdf2_sha256$390000$Lf00MpxxWpS4dE8973XhLI$hFKD9Cf1NA57kTmL2RXf3IoRuv+PFw8trLINrCce+C8=', '2023-03-20 05:48:28.615288', 0, 'aditi', 'aditi', 'maskey', 'aditi@gmail.com', 0, 1, '2023-02-23 10:54:44.699113'),
(46, 'pbkdf2_sha256$390000$bltsqEeDIMTSb6FcL9SJq2$Tc77gZ3avK/6yNdIojZPFft1aRRS7j/vL0ZukcKfOWA=', '2023-03-24 09:50:54.491602', 0, 'Yubraj', 'Yubraj', 'Khadka', 'khadkayubraj239@gmail.com', 0, 1, '2023-02-23 16:00:08.167699'),
(47, 'pbkdf2_sha256$390000$mGlKmZyVjVoZkTukokmRoD$rYYll4tFyT7L05Kjq4jTnCkjFqAelvT3ccPmMiqL8l8=', '2023-02-24 01:40:45.925697', 0, 'Jasz', 'jasmine', 'neupane', 'jasmineneupane@kcc.edu.np', 0, 1, '2023-02-24 01:40:32.283360'),
(48, 'pbkdf2_sha256$390000$tzSyj0jKzlQIEOHxIeS9fh$966htDErOx+7BSsa/A4d2/xrf3mTvtZN4e0cQMujIW0=', '2023-03-19 10:50:16.193669', 0, 'nabaraj', 'nabaraj', 'khadka', 'nabaraj@gmail.com', 0, 1, '2023-03-19 10:49:36.538349'),
(49, 'pbkdf2_sha256$390000$c5HP1NcIPMYVzQAcUN65bp$RzriZwdiPeols83idYvLb7W74xhxLjMrPyTx5Cir0sc=', '2023-03-20 07:31:30.806682', 0, 'aditi2', 'aditi', 'maskey', 'aditi@gmail.com', 0, 1, '2023-03-20 07:31:04.180524'),
(50, 'pbkdf2_sha256$390000$79rKjB8Lt6XeRP0w9gqyTL$jFYC93o8H7Lb+zE2opS1EZBin5fbXiA6V0NfZQK2nw4=', '2023-03-21 04:01:42.460276', 0, 'sigdel', 'sigdel', 'prabati', 'parbati@gmail.com', 0, 1, '2023-03-21 04:01:31.651050'),
(55, 'pbkdf2_sha256$390000$CmaeYPfNbhkqhfr7Srbr1W$kwIGxi288neRc/Z103l+JD5i13ADtgu9ABphdoQ9gO0=', NULL, 0, 'ishwor', 'ishwor', 'khadka', 'a@a.com', 0, 1, '2023-03-21 04:48:49.685189'),
(56, 'pbkdf2_sha256$390000$z6gb9L8uEh1dTiEcgCaVED$XsV2EhC7GHit8IiyEcTb5+1INT7d8BF5w0cRfgs43Vk=', NULL, 0, 'sigdel11', 'prabhat', 'sigdel', 'Prabhat@gmail.com', 0, 1, '2023-03-21 04:56:43.935031'),
(59, 'pbkdf2_sha256$390000$nhTBNA40QDwPW9kVDlZE9U$TzbNXqNlSGy8oEYjNNGE+McaiN3lXy5eqklaqoZMYoI=', '2023-03-24 14:49:12.644417', 0, 'AditiM', 'ads', 'm', 'ads@gmail.com', 0, 1, '2023-03-24 14:49:01.467297'),
(60, 'pbkdf2_sha256$390000$1AKnZxmwVKRDlX4KckCH66$u9oeRIstFrxsISgclAH+qBy8UFZzS1TMtb/kKHJBHFo=', '2023-03-24 16:10:09.833706', 0, 'Aditi@M', 'ads', 'm', 'ads@gmail.com', 0, 1, '2023-03-24 14:52:15.861355');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `captcha_captchastore`
--

CREATE TABLE `captcha_captchastore` (
  `id` int(11) NOT NULL,
  `challenge` varchar(32) NOT NULL,
  `response` varchar(32) NOT NULL,
  `hashkey` varchar(40) NOT NULL,
  `expiration` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `captcha_captchastore`
--

INSERT INTO `captcha_captchastore` (`id`, `challenge`, `response`, `hashkey`, `expiration`) VALUES
(280, 'RJBA', 'rjba', '99f87ea3ec0d40acaa58c380e84811d7b759b1d3', '2023-03-24 15:56:12.086165'),
(281, 'OLVF', 'olvf', '9d8041af662ec68a8c32e7ac15c6f00db9be9e4f', '2023-03-24 15:56:12.433236'),
(283, 'GRRY', 'grry', '91343852468e2d7dd001b61734dc9c3b1ce3b4cf', '2023-03-24 15:57:32.932149');

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2023-02-08 08:49:07.722512', '1', 'Mandala', 1, '[{\"added\": {}}]', 11, 1),
(2, '2023-02-08 08:49:19.334856', '2', 'Drawing', 1, '[{\"added\": {}}]', 11, 1),
(3, '2023-02-08 08:49:28.103616', '3', 'Sketch', 1, '[{\"added\": {}}]', 11, 1),
(4, '2023-02-08 08:49:37.915427', '4', 'Painting', 1, '[{\"added\": {}}]', 11, 1),
(5, '2023-02-08 09:55:36.642810', '1', 'Mandala1', 1, '[{\"added\": {}}]', 13, 1),
(6, '2023-02-08 09:56:58.738125', '2', 'Drawing1', 1, '[{\"added\": {}}]', 13, 1),
(7, '2023-02-08 09:58:15.720123', '3', 'Sketch1', 1, '[{\"added\": {}}]', 13, 1),
(8, '2023-02-08 09:58:54.969318', '4', 'Painting1', 1, '[{\"added\": {}}]', 13, 1),
(9, '2023-02-08 09:59:35.849131', '5', 'Painting2', 1, '[{\"added\": {}}]', 13, 1),
(10, '2023-02-08 10:00:19.645345', '6', 'Drawing2', 1, '[{\"added\": {}}]', 13, 1),
(11, '2023-02-08 10:01:09.223800', '7', 'Mandala2', 1, '[{\"added\": {}}]', 13, 1),
(12, '2023-02-15 15:34:14.944906', '10', 'maskey', 2, '[{\"changed\": {\"fields\": [\"First name\", \"Last name\"]}}]', 4, 1),
(13, '2023-02-15 15:40:08.929624', '1', 'aditi maskey', 1, '[{\"added\": {}}]', 17, 1),
(14, '2023-02-16 10:16:17.212951', '8', 'Drawing3', 1, '[{\"added\": {}}]', 13, 1),
(15, '2023-02-16 10:17:07.918399', '9', 'Drawing4', 1, '[{\"added\": {}}]', 13, 1),
(16, '2023-02-16 10:18:07.173540', '10', 'Drawing7', 1, '[{\"added\": {}}]', 13, 1),
(17, '2023-02-16 10:51:51.953084', '7', 'Mandala2', 2, '[{\"changed\": {\"fields\": [\"Stock\"]}}]', 13, 1),
(18, '2023-02-16 13:59:48.616223', '11', 'Mandala3', 1, '[{\"added\": {}}]', 13, 1),
(19, '2023-02-17 13:25:37.095880', '15', 'sarita', 3, '', 4, 1),
(20, '2023-02-17 13:48:27.733266', '3', 'aditi', 3, '', 4, 1),
(21, '2023-02-17 13:48:38.048875', '18', 'bishnu', 3, '', 4, 1),
(22, '2023-02-17 13:49:22.690224', '17', 'indira', 3, '', 4, 1),
(23, '2023-02-17 13:49:32.567732', '4', 'khadka', 3, '', 4, 1),
(24, '2023-02-17 13:49:40.476851', '6', 'khadka1', 3, '', 4, 1),
(25, '2023-02-17 13:50:12.139815', '7', 'khadka11', 3, '', 4, 1),
(26, '2023-02-17 13:50:21.926569', '9', 'khadka111', 3, '', 4, 1),
(27, '2023-02-17 13:50:31.791434', '12', 'mack', 3, '', 4, 1),
(28, '2023-02-17 13:50:49.806438', '10', 'maskey', 3, '', 4, 1),
(29, '2023-02-17 13:50:58.423114', '5', 'melamchi', 3, '', 4, 1),
(30, '2023-02-17 13:51:09.544933', '11', 'nabaraj', 3, '', 4, 1),
(31, '2023-02-17 13:51:18.796890', '16', 'sarita1', 3, '', 4, 1),
(32, '2023-02-17 13:51:33.231486', '13', 'ram', 3, '', 4, 1),
(33, '2023-02-17 13:51:41.394917', '8', 'yubraj11', 3, '', 4, 1),
(34, '2023-02-19 04:15:21.311880', '1', 'Yubraj Khadka', 1, '[{\"added\": {}}]', 17, 1),
(35, '2023-02-19 07:33:59.555556', '36', 'krishna1', 3, '', 4, 1),
(36, '2023-02-19 07:34:07.995677', '33', 'indira', 3, '', 4, 1),
(37, '2023-02-19 07:34:17.961481', '30', 'ram', 3, '', 4, 1),
(38, '2023-02-19 07:34:57.592954', '29', 'yubraj', 3, '', 4, 1),
(39, '2023-02-20 12:35:36.985015', '1', 'yubraj', 2, '[{\"changed\": {\"fields\": [\"Amount\", \"Paid\"]}}]', 19, 1),
(40, '2023-02-20 15:16:32.699535', '6', 'yubraj', 3, '', 19, 1),
(41, '2023-02-20 15:16:39.960179', '12', 'yubraj', 3, '', 19, 1),
(42, '2023-02-20 15:16:47.346687', '11', 'yubraj', 3, '', 19, 1),
(43, '2023-02-20 15:16:53.643604', '10', 'yubraj', 3, '', 19, 1),
(44, '2023-02-20 15:17:00.428399', '9', 'admin', 3, '', 19, 1),
(45, '2023-02-20 15:17:24.332737', '7', 'admin', 3, '', 19, 1),
(46, '2023-02-20 15:32:29.635973', '15', 'yubraj', 2, '[{\"changed\": {\"fields\": [\"Paid\"]}}]', 19, 1),
(47, '2023-02-20 15:58:09.950135', '18', 'admin', 3, '', 19, 1),
(48, '2023-02-20 15:58:09.956159', '16', 'aditi@', 3, '', 19, 1),
(49, '2023-02-21 09:31:42.034439', '27', 'yubraj', 3, '', 19, 1),
(50, '2023-02-21 09:31:42.042827', '23', 'admin', 3, '', 19, 1),
(51, '2023-02-21 09:31:42.042827', '21', 'yubraj', 3, '', 19, 1),
(52, '2023-02-21 09:31:42.042827', '19', 'admin', 3, '', 19, 1),
(53, '2023-02-21 09:31:42.051209', '14', 'admin', 3, '', 19, 1),
(54, '2023-02-21 10:27:30.837864', '45', 'yubraj', 3, '', 19, 1),
(55, '2023-02-21 10:27:30.841147', '44', 'yubraj', 3, '', 19, 1),
(56, '2023-02-21 10:27:30.843940', '43', 'yubraj', 3, '', 19, 1),
(57, '2023-02-21 10:27:30.848875', '42', 'yubraj', 3, '', 19, 1),
(58, '2023-02-21 10:27:30.851140', '41', 'yubraj', 3, '', 19, 1),
(59, '2023-02-21 10:27:30.853142', '40', 'yubraj', 3, '', 19, 1),
(60, '2023-02-21 10:27:30.854497', '39', 'yubraj', 3, '', 19, 1),
(61, '2023-02-21 10:27:30.857506', '38', 'yubraj', 3, '', 19, 1),
(62, '2023-02-21 10:27:30.859607', '37', 'yubraj', 3, '', 19, 1),
(63, '2023-02-21 10:27:30.861777', '36', 'yubraj', 3, '', 19, 1),
(64, '2023-02-21 10:27:30.863680', '35', 'yubraj', 3, '', 19, 1),
(65, '2023-02-21 10:27:30.866507', '34', 'yubraj', 3, '', 19, 1),
(66, '2023-02-21 10:27:30.869771', '33', 'yubraj', 3, '', 19, 1),
(67, '2023-02-21 10:27:30.871868', '32', 'yubraj', 3, '', 19, 1),
(68, '2023-02-21 10:27:30.873871', '31', 'yubraj', 3, '', 19, 1),
(69, '2023-02-21 10:27:30.875870', '30', 'yubraj', 3, '', 19, 1),
(70, '2023-02-21 10:27:30.878972', '29', 'yubraj', 3, '', 19, 1),
(71, '2023-02-21 10:27:30.881137', '28', 'yubraj', 3, '', 19, 1),
(72, '2023-02-21 10:27:30.882868', '26', 'yubraj', 3, '', 19, 1),
(73, '2023-02-21 10:27:30.886140', '25', 'yubraj', 3, '', 19, 1),
(74, '2023-02-21 10:27:30.888528', '24', 'yubraj', 3, '', 19, 1),
(75, '2023-02-21 10:27:30.890528', '22', 'admin', 3, '', 19, 1),
(76, '2023-02-21 10:27:30.892600', '20', 'yubraj', 3, '', 19, 1),
(77, '2023-02-21 10:27:30.894796', '17', 'admin', 3, '', 19, 1),
(78, '2023-02-21 10:27:30.896803', '15', 'yubraj', 3, '', 19, 1),
(79, '2023-02-21 10:27:30.898528', '13', 'yubraj', 3, '', 19, 1),
(80, '2023-02-21 15:50:29.399910', '68', 'admin', 3, '', 19, 1),
(81, '2023-02-21 15:50:29.404248', '67', 'admin', 3, '', 19, 1),
(82, '2023-02-21 15:50:29.406347', '66', 'admin', 3, '', 19, 1),
(83, '2023-02-21 15:50:29.411870', '65', 'yubraj', 3, '', 19, 1),
(84, '2023-02-21 15:50:29.411870', '64', 'yubraj', 3, '', 19, 1),
(85, '2023-02-21 15:50:29.411870', '63', 'yubraj', 3, '', 19, 1),
(86, '2023-02-21 15:50:29.419887', '62', 'yubraj', 3, '', 19, 1),
(87, '2023-02-21 15:50:29.424936', '61', 'yubraj', 3, '', 19, 1),
(88, '2023-02-21 15:50:29.427125', '60', 'yubraj', 3, '', 19, 1),
(89, '2023-02-21 15:50:29.429852', '59', 'yubraj', 3, '', 19, 1),
(90, '2023-02-21 15:50:29.429852', '58', 'yubraj', 3, '', 19, 1),
(91, '2023-02-21 15:50:29.440506', '57', 'yubraj', 3, '', 19, 1),
(92, '2023-02-21 15:50:29.442199', '56', 'yubraj', 3, '', 19, 1),
(93, '2023-02-21 15:50:29.444785', '55', 'yubraj', 3, '', 19, 1),
(94, '2023-02-21 15:50:29.444785', '54', 'yubraj', 3, '', 19, 1),
(95, '2023-02-21 15:50:29.444785', '53', 'yubraj', 3, '', 19, 1),
(96, '2023-02-21 15:50:29.455485', '52', 'yubraj', 3, '', 19, 1),
(97, '2023-02-21 15:50:29.455485', '51', 'yubraj', 3, '', 19, 1),
(98, '2023-02-21 15:50:29.461506', '50', 'yubraj', 3, '', 19, 1),
(99, '2023-02-21 15:50:29.461506', '49', 'yubraj', 3, '', 19, 1),
(100, '2023-02-21 15:50:29.469520', '48', 'yubraj', 3, '', 19, 1),
(101, '2023-02-21 15:50:29.471251', '47', 'yubraj', 3, '', 19, 1),
(102, '2023-02-21 15:50:29.471251', '46', 'yubraj', 3, '', 19, 1),
(103, '2023-02-22 11:22:59.003430', '114', 'yubraj', 2, '[{\"changed\": {\"name\": \"order item\", \"object\": \"yubraj\", \"fields\": [\"Image\"]}}]', 19, 1),
(104, '2023-02-22 11:23:30.313072', '114', 'yubraj', 2, '[{\"changed\": {\"name\": \"order item\", \"object\": \"yubraj\", \"fields\": [\"Image\"]}}]', 19, 1),
(105, '2023-02-23 10:52:34.350572', '140', 'yubraj', 3, '', 19, 1),
(106, '2023-02-23 10:52:34.354894', '139', 'yubraj', 3, '', 19, 1),
(107, '2023-02-23 10:52:34.356573', '138', 'yubraj', 3, '', 19, 1),
(108, '2023-02-23 10:52:34.359776', '137', 'yubraj', 3, '', 19, 1),
(109, '2023-02-23 10:52:34.361574', '136', 'yubraj', 3, '', 19, 1),
(110, '2023-02-23 10:52:34.364848', '135', 'yubraj', 3, '', 19, 1),
(111, '2023-02-23 10:52:34.366931', '134', 'yubraj', 3, '', 19, 1),
(112, '2023-02-23 10:52:34.369108', '133', 'yubraj', 3, '', 19, 1),
(113, '2023-02-23 10:52:34.371061', '132', 'yubraj', 3, '', 19, 1),
(114, '2023-02-23 10:52:34.373191', '131', 'yubraj', 3, '', 19, 1),
(115, '2023-02-23 10:52:34.375042', '130', 'yubraj', 3, '', 19, 1),
(116, '2023-02-23 10:52:34.376955', '129', 'yubraj', 3, '', 19, 1),
(117, '2023-02-23 10:52:34.378930', '128', 'yubraj', 3, '', 19, 1),
(118, '2023-02-23 10:52:34.381053', '127', 'yubraj', 3, '', 19, 1),
(119, '2023-02-23 10:52:34.384944', '126', 'yubraj', 3, '', 19, 1),
(120, '2023-02-23 10:52:34.386944', '125', 'admin', 3, '', 19, 1),
(121, '2023-02-23 10:52:34.389246', '124', 'admin', 3, '', 19, 1),
(122, '2023-02-23 10:52:34.391248', '123', 'admin', 3, '', 19, 1),
(123, '2023-02-23 10:52:34.392968', '122', 'admin', 3, '', 19, 1),
(124, '2023-02-23 10:52:34.394945', '121', 'admin', 3, '', 19, 1),
(125, '2023-02-23 10:52:34.396947', '120', 'admin', 3, '', 19, 1),
(126, '2023-02-23 10:52:34.399091', '119', 'admin', 3, '', 19, 1),
(127, '2023-02-23 10:52:34.399731', '118', 'admin', 3, '', 19, 1),
(128, '2023-02-23 10:52:34.401777', '117', 'admin', 3, '', 19, 1),
(129, '2023-02-23 10:52:34.402739', '116', 'admin', 3, '', 19, 1),
(130, '2023-02-23 10:52:34.405003', '115', 'admin', 3, '', 19, 1),
(131, '2023-02-23 10:52:34.405742', '114', 'yubraj', 3, '', 19, 1),
(132, '2023-02-23 10:52:34.407740', '113', 'admin', 3, '', 19, 1),
(133, '2023-02-23 10:52:34.410740', '112', 'yubraj', 3, '', 19, 1),
(134, '2023-02-23 10:52:34.411742', '111', 'yubraj', 3, '', 19, 1),
(135, '2023-02-23 10:52:34.413740', '110', 'yubraj', 3, '', 19, 1),
(136, '2023-02-23 10:52:34.415740', '109', 'yubraj', 3, '', 19, 1),
(137, '2023-02-23 10:52:34.417761', '108', 'yubraj', 3, '', 19, 1),
(138, '2023-02-23 10:52:34.419770', '107', 'yubraj', 3, '', 19, 1),
(139, '2023-02-23 10:52:34.422040', '106', 'yubraj', 3, '', 19, 1),
(140, '2023-02-23 10:52:34.423852', '105', 'yubraj', 3, '', 19, 1),
(141, '2023-02-23 10:52:34.426038', '104', 'yubraj', 3, '', 19, 1),
(142, '2023-02-23 10:52:34.427772', '103', 'yubraj', 3, '', 19, 1),
(143, '2023-02-23 10:52:34.429806', '102', 'yubraj', 3, '', 19, 1),
(144, '2023-02-23 10:52:34.431887', '101', 'yubraj', 3, '', 19, 1),
(145, '2023-02-23 10:52:34.434018', '100', 'yubraj', 3, '', 19, 1),
(146, '2023-02-23 10:52:34.435882', '99', 'yubraj', 3, '', 19, 1),
(147, '2023-02-23 10:52:34.437216', '98', 'yubraj', 3, '', 19, 1),
(148, '2023-02-23 10:52:34.439217', '97', 'yubraj', 3, '', 19, 1),
(149, '2023-02-23 10:52:34.441218', '96', 'yubraj', 3, '', 19, 1),
(150, '2023-02-23 10:52:34.442215', '95', 'yubraj', 3, '', 19, 1),
(151, '2023-02-23 10:52:34.444216', '94', 'yubraj', 3, '', 19, 1),
(152, '2023-02-23 10:52:34.445581', '93', 'yubraj', 3, '', 19, 1),
(153, '2023-02-23 10:52:34.447591', '92', 'yubraj', 3, '', 19, 1),
(154, '2023-02-23 10:52:34.448860', '91', 'yubraj', 3, '', 19, 1),
(155, '2023-02-23 10:52:34.450591', '90', 'yubraj', 3, '', 19, 1),
(156, '2023-02-23 10:52:34.451616', '89', 'yubraj', 3, '', 19, 1),
(157, '2023-02-23 10:52:34.453589', '88', 'yubraj', 3, '', 19, 1),
(158, '2023-02-23 10:52:34.455589', '87', 'yubraj', 3, '', 19, 1),
(159, '2023-02-23 10:52:34.456614', '86', 'yubraj', 3, '', 19, 1),
(160, '2023-02-23 10:52:34.458605', '85', 'yubraj', 3, '', 19, 1),
(161, '2023-02-23 10:52:34.459590', '84', 'yubraj', 3, '', 19, 1),
(162, '2023-02-23 10:52:34.460590', '83', 'yubraj', 3, '', 19, 1),
(163, '2023-02-23 10:52:34.462591', '82', 'yubraj', 3, '', 19, 1),
(164, '2023-02-23 10:52:34.464590', '81', 'yubraj', 3, '', 19, 1),
(165, '2023-02-23 10:52:34.465590', '80', 'yubraj', 3, '', 19, 1),
(166, '2023-02-23 10:52:34.466590', '79', 'yubraj', 3, '', 19, 1),
(167, '2023-02-23 10:52:34.468705', '78', 'yubraj', 3, '', 19, 1),
(168, '2023-02-23 10:52:34.469757', '77', 'yubraj', 3, '', 19, 1),
(169, '2023-02-23 10:52:34.471756', '76', 'yubraj', 3, '', 19, 1),
(170, '2023-02-23 10:52:34.473019', '75', 'yubraj', 3, '', 19, 1),
(171, '2023-02-23 10:52:34.473755', '74', 'yubraj', 3, '', 19, 1),
(172, '2023-02-23 10:52:34.475922', '73', 'yubraj', 3, '', 19, 1),
(173, '2023-02-23 10:52:34.476756', '72', 'yubraj', 3, '', 19, 1),
(174, '2023-02-23 10:52:34.477756', '71', 'admin', 3, '', 19, 1),
(175, '2023-02-23 10:52:34.479758', '70', 'admin', 3, '', 19, 1),
(176, '2023-02-23 10:52:34.481756', '69', 'admin', 3, '', 19, 1),
(177, '2023-02-23 11:08:02.461676', '141', 'admin', 2, '[{\"changed\": {\"fields\": [\"Order status\", \"Paid\"]}}]', 19, 1),
(178, '2023-02-23 15:59:20.774379', '43', 'yubraj', 3, '', 4, 1),
(179, '2023-02-24 11:18:04.504976', '5', 'Painting2', 2, '[{\"changed\": {\"fields\": [\"Stock\"]}}]', 13, 1),
(180, '2023-02-24 11:31:30.499578', '157', 'Yubraj', 3, '', 19, 1),
(181, '2023-02-24 11:31:47.713137', '156', 'Yubraj', 3, '', 19, 1),
(182, '2023-02-24 11:31:47.718210', '155', 'Yubraj', 3, '', 19, 1),
(183, '2023-02-24 11:31:47.720922', '154', 'Yubraj', 3, '', 19, 1),
(184, '2023-02-24 11:31:47.723918', '153', 'Yubraj', 3, '', 19, 1),
(185, '2023-02-24 11:31:47.725914', '152', 'Jasz', 3, '', 19, 1),
(186, '2023-02-24 11:31:47.728150', '151', 'Yubraj', 3, '', 19, 1),
(187, '2023-02-24 11:31:47.730212', '150', 'Yubraj', 3, '', 19, 1),
(188, '2023-02-24 11:31:47.732558', '149', 'Yubraj', 3, '', 19, 1),
(189, '2023-02-24 11:31:47.734559', '148', 'aditi', 3, '', 19, 1),
(190, '2023-02-24 11:31:47.736604', '147', 'Yubraj', 3, '', 19, 1),
(191, '2023-02-24 11:31:47.738562', '144', 'aditi', 3, '', 19, 1),
(192, '2023-02-24 11:31:47.741563', '143', 'aditi', 3, '', 19, 1),
(193, '2023-02-24 11:31:47.743560', '142', 'aditi', 3, '', 19, 1),
(194, '2023-02-24 11:31:47.745733', '141', 'admin', 3, '', 19, 1),
(195, '2023-02-24 12:54:34.688445', '178', 'Yubraj', 2, '[{\"changed\": {\"fields\": [\"Order status\", \"Paid\"]}}]', 19, 1),
(196, '2023-02-25 16:14:22.385632', '180', 'admin', 3, '', 19, 1),
(197, '2023-02-25 16:14:22.395758', '179', 'admin', 3, '', 19, 1),
(198, '2023-02-25 16:14:22.397592', '178', 'Yubraj', 3, '', 19, 1),
(199, '2023-02-25 16:14:22.399790', '177', 'Yubraj', 3, '', 19, 1),
(200, '2023-02-25 16:14:22.403621', '176', 'Yubraj', 3, '', 19, 1),
(201, '2023-02-25 16:14:22.405618', '175', 'Yubraj', 3, '', 19, 1),
(202, '2023-02-25 16:14:22.408173', '174', 'Yubraj', 3, '', 19, 1),
(203, '2023-02-25 16:14:22.411373', '173', 'Yubraj', 3, '', 19, 1),
(204, '2023-02-25 16:14:22.413156', '172', 'admin', 3, '', 19, 1),
(205, '2023-02-25 16:14:22.415410', '171', 'Yubraj', 3, '', 19, 1),
(206, '2023-02-25 16:14:22.419152', '170', 'Yubraj', 3, '', 19, 1),
(207, '2023-02-25 16:14:22.422175', '169', 'Yubraj', 3, '', 19, 1),
(208, '2023-02-25 16:14:22.425073', '168', 'Yubraj', 3, '', 19, 1),
(209, '2023-02-25 16:14:22.428907', '167', 'Yubraj', 3, '', 19, 1),
(210, '2023-02-25 16:14:22.430906', '166', 'Yubraj', 3, '', 19, 1),
(211, '2023-02-25 16:14:22.433180', '165', 'Yubraj', 3, '', 19, 1),
(212, '2023-02-25 16:14:22.435906', '164', 'Yubraj', 3, '', 19, 1),
(213, '2023-02-25 16:14:22.438909', '163', 'Yubraj', 3, '', 19, 1),
(214, '2023-02-25 16:14:22.440764', '162', 'Yubraj', 3, '', 19, 1),
(215, '2023-02-25 16:14:22.443774', '161', 'Yubraj', 3, '', 19, 1),
(216, '2023-02-25 16:14:22.445773', '160', 'Yubraj', 3, '', 19, 1),
(217, '2023-02-25 16:14:22.447773', '159', 'Yubraj', 3, '', 19, 1),
(218, '2023-02-25 16:14:22.450773', '158', 'Yubraj', 3, '', 19, 1),
(219, '2023-02-25 16:18:27.739518', '182', 'aditi', 2, '[{\"changed\": {\"fields\": [\"Paid\"]}}]', 19, 1),
(220, '2023-02-25 16:20:37.311935', '183', 'Yubraj', 2, '[{\"changed\": {\"fields\": [\"Paid\"]}}]', 19, 1),
(221, '2023-03-10 14:51:41.099873', '1', 'Drawing1', 1, '[{\"added\": {}}]', 12, 1),
(222, '2023-03-10 14:52:27.048955', '1', 'Drawing1', 3, '', 12, 1),
(223, '2023-03-13 07:50:41.490399', '192', 'Yubraj', 2, '[{\"changed\": {\"fields\": [\"Order status\"]}}]', 19, 1),
(224, '2023-03-13 12:14:46.359127', '1', 'Yubraj', 1, '[{\"added\": {}}]', 21, 1),
(225, '2023-03-13 12:18:10.757178', '192', 'Yubraj', 2, '[{\"changed\": {\"fields\": [\"Order status\"]}}]', 19, 1),
(226, '2023-03-13 15:32:14.482925', '8', 'jasmine', 3, '', 11, 1),
(227, '2023-03-13 15:32:14.486957', '7', 'aditi', 3, '', 11, 1),
(228, '2023-03-13 15:32:14.489497', '5', 'yubraj', 3, '', 11, 1),
(229, '2023-03-13 16:39:44.565649', '10', 'Yubraj', 3, '', 11, 1),
(230, '2023-03-13 16:39:44.571923', '9', 'jasmine', 3, '', 11, 1),
(231, '2023-03-13 16:40:47.835255', '12', 'yubraj', 3, '', 11, 1),
(232, '2023-03-13 16:47:09.605471', '13', 'yubraj', 3, '', 11, 1),
(233, '2023-03-14 10:58:27.270253', '14', 'aditi', 3, '', 11, 1),
(234, '2023-03-14 12:56:29.840983', '192', 'Yubraj', 2, '[{\"changed\": {\"fields\": [\"Order status\"]}}]', 19, 1),
(235, '2023-03-14 12:58:15.591515', '182', 'aditi', 2, '[{\"changed\": {\"fields\": [\"Order status\"]}}]', 19, 1),
(236, '2023-03-14 13:53:35.520752', '188', 'Yubraj', 2, '[{\"changed\": {\"fields\": [\"Order status\", \"Paid\"]}}]', 19, 1),
(237, '2023-03-15 04:20:29.032993', '197', 'admin', 3, '', 19, 1),
(238, '2023-03-15 04:20:29.036443', '196', 'admin', 3, '', 19, 1),
(239, '2023-03-15 04:20:29.038454', '195', 'admin', 3, '', 19, 1),
(240, '2023-03-15 04:21:01.020125', '198', 'admin', 3, '', 19, 1),
(241, '2023-03-15 04:22:04.400662', '201', 'Yubraj', 2, '[{\"changed\": {\"name\": \"order item\", \"object\": \"Yubraj\", \"fields\": [\"Image\"]}}]', 19, 1),
(242, '2023-03-15 04:37:19.909916', '203', 'admin', 2, '[{\"changed\": {\"fields\": [\"Firstname\", \"Phone\", \"Email\"]}}]', 19, 1),
(243, '2023-03-15 04:45:51.628520', '204', 'aditi', 2, '[{\"changed\": {\"name\": \"order item\", \"object\": \"aditi\", \"fields\": [\"Image\"]}}]', 19, 1),
(244, '2023-03-15 05:05:40.841559', '205', 'admin', 3, '', 19, 1),
(245, '2023-03-17 08:04:58.193405', '12', 'nabaraj', 3, '', 13, 1),
(246, '2023-03-17 14:25:07.875607', '1', 'Yubraj', 2, '[{\"changed\": {\"fields\": [\"Full name\"]}}]', 21, 1),
(247, '2023-03-17 14:26:19.359951', '1', 'Yubraj', 2, '[{\"changed\": {\"fields\": [\"Full name\"]}}]', 21, 1),
(248, '2023-03-17 14:28:35.522264', '2', 'aditi', 1, '[{\"added\": {}}]', 21, 1),
(249, '2023-03-17 16:20:52.574771', '15', 'Mandala1', 1, '[{\"added\": {}}]', 13, 1),
(250, '2023-03-17 16:21:38.176944', '16', 'Mandala2', 1, '[{\"added\": {}}]', 13, 1),
(251, '2023-03-17 16:22:24.115126', '17', 'Mandala3', 1, '[{\"added\": {}}]', 13, 1),
(252, '2023-03-17 16:24:12.224700', '18', 'Sketch2', 1, '[{\"added\": {}}]', 13, 1),
(253, '2023-03-17 16:24:52.042867', '19', 'Sketch3', 1, '[{\"added\": {}}]', 13, 1),
(254, '2023-03-17 16:26:04.417411', '20', 'Painting2', 1, '[{\"added\": {}}]', 13, 1),
(255, '2023-03-17 16:26:47.120171', '21', 'Painting3', 1, '[{\"added\": {}}]', 13, 1),
(256, '2023-03-19 10:58:01.381274', '214', 'Yubraj', 3, '', 19, 1),
(257, '2023-03-19 10:58:01.397143', '211', 'Yubraj', 3, '', 19, 1),
(258, '2023-03-19 10:58:01.397143', '208', 'Yubraj', 3, '', 19, 1),
(259, '2023-03-19 10:58:01.397143', '207', 'Yubraj', 3, '', 19, 1),
(260, '2023-03-19 10:58:01.397143', '202', 'Yubraj', 3, '', 19, 1),
(261, '2023-03-19 10:58:01.397143', '200', 'Yubraj', 3, '', 19, 1),
(262, '2023-03-19 10:58:01.415955', '193', 'Yubraj', 3, '', 19, 1),
(263, '2023-03-19 10:58:01.415955', '192', 'Yubraj', 3, '', 19, 1),
(264, '2023-03-19 10:58:01.415955', '191', 'Yubraj', 3, '', 19, 1),
(265, '2023-03-19 10:58:01.415955', '188', 'Yubraj', 3, '', 19, 1),
(266, '2023-03-19 10:58:01.415955', '187', 'Yubraj', 3, '', 19, 1),
(267, '2023-03-19 10:58:01.428468', '186', 'Yubraj', 3, '', 19, 1),
(268, '2023-03-19 10:58:01.428468', '185', 'Yubraj', 3, '', 19, 1),
(269, '2023-03-19 10:58:01.428468', '184', 'Yubraj', 3, '', 19, 1),
(270, '2023-03-19 10:58:01.428468', '183', 'Yubraj', 3, '', 19, 1),
(271, '2023-03-19 11:29:59.488263', '2', 'aditi', 3, '', 21, 1),
(272, '2023-03-21 04:38:59.017346', '51', '11', 3, '', 4, 1),
(273, '2023-03-21 04:38:59.033282', '53', 'khatri', 3, '', 4, 1),
(274, '2023-03-21 04:38:59.033282', '54', 'prajita11', 3, '', 4, 1),
(275, '2023-03-21 11:08:01.334803', '52', 'yubraj11', 3, '', 4, 1),
(276, '2023-03-21 11:08:01.334803', '57', 'yubraj111', 3, '', 4, 1),
(277, '2023-03-21 11:08:01.348923', '58', 'yubraj1111', 3, '', 4, 1),
(278, '2023-03-24 15:54:48.934757', '3', 'Aditi@M', 1, '[{\"added\": {}}]', 21, 1),
(279, '2023-03-24 16:03:54.478714', '258', 'Yubraj', 3, '', 19, 1),
(280, '2023-03-24 16:03:54.489684', '257', 'Yubraj', 3, '', 19, 1),
(281, '2023-03-24 16:03:54.495672', '256', 'Yubraj', 3, '', 19, 1),
(282, '2023-03-24 16:03:54.501651', '255', 'Yubraj', 3, '', 19, 1),
(283, '2023-03-24 16:03:54.507635', '254', 'Yubraj', 3, '', 19, 1),
(284, '2023-03-24 16:05:20.716011', '260', 'Yubraj', 3, '', 19, 1),
(285, '2023-03-24 16:05:20.722992', '259', 'Yubraj', 3, '', 19, 1),
(286, '2023-03-24 16:05:20.727977', '253', 'Yubraj', 3, '', 19, 1),
(287, '2023-03-24 16:05:20.734960', '252', 'Yubraj', 3, '', 19, 1),
(288, '2023-03-24 16:05:20.741942', '251', 'Yubraj', 3, '', 19, 1),
(289, '2023-03-24 16:05:20.747925', '250', 'Yubraj', 3, '', 19, 1),
(290, '2023-03-24 16:05:20.753911', '249', 'Yubraj', 3, '', 19, 1),
(291, '2023-03-24 16:05:20.759893', '248', 'Yubraj', 3, '', 19, 1),
(292, '2023-03-24 16:05:20.765879', '246', 'Yubraj', 3, '', 19, 1),
(293, '2023-03-24 16:05:20.771861', '245', 'Yubraj', 3, '', 19, 1),
(294, '2023-03-24 16:05:20.777847', '244', 'Yubraj', 3, '', 19, 1),
(295, '2023-03-24 16:05:20.785825', '243', 'Yubraj', 3, '', 19, 1),
(296, '2023-03-24 16:05:20.792804', '242', 'Yubraj', 3, '', 19, 1),
(297, '2023-03-24 16:05:20.803775', '241', 'Yubraj', 3, '', 19, 1),
(298, '2023-03-24 16:05:20.810756', '240', 'Yubraj', 3, '', 19, 1),
(299, '2023-03-24 16:05:20.817738', '239', 'Yubraj', 3, '', 19, 1),
(300, '2023-03-24 16:05:20.828710', '238', 'Yubraj', 3, '', 19, 1),
(301, '2023-03-24 16:05:20.835690', '237', 'Yubraj', 3, '', 19, 1),
(302, '2023-03-24 16:05:20.841675', '236', 'Yubraj', 3, '', 19, 1),
(303, '2023-03-24 16:05:20.847658', '235', 'Yubraj', 3, '', 19, 1),
(304, '2023-03-24 16:05:20.854640', '234', 'Yubraj', 3, '', 19, 1),
(305, '2023-03-24 16:05:20.859626', '233', 'Yubraj', 3, '', 19, 1),
(306, '2023-03-24 16:05:20.867605', '228', 'Yubraj', 3, '', 19, 1),
(307, '2023-03-24 16:05:20.876584', '225', 'Yubraj', 3, '', 19, 1),
(308, '2023-03-24 16:05:20.881567', '219', 'Yubraj', 3, '', 19, 1),
(309, '2023-03-24 16:05:20.886558', '218', 'Yubraj', 3, '', 19, 1),
(310, '2023-03-24 16:05:20.892538', '216', 'Yubraj', 3, '', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(21, 'art', 'admin'),
(15, 'art', 'cart'),
(11, 'art', 'category'),
(16, 'art', 'contact'),
(17, 'art', 'customer'),
(19, 'art', 'order'),
(18, 'art', 'orderitem'),
(13, 'art', 'product'),
(12, 'art', 'productimage'),
(22, 'art', 'rating'),
(14, 'art', 'wishlist'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(20, 'captcha', 'captchastore'),
(5, 'contenttypes', 'contenttype'),
(7, 'Projectapp', 'customer'),
(8, 'Projectapp', 'customerorder'),
(6, 'sessions', 'session'),
(9, 'shop', 'category'),
(10, 'shop', 'product');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'shop', '0001_initial', '2023-02-08 05:39:23.858093'),
(2, 'Projectapp', '0001_initial', '2023-02-08 05:39:24.009427'),
(3, 'contenttypes', '0001_initial', '2023-02-08 05:39:24.055564'),
(4, 'auth', '0001_initial', '2023-02-08 05:39:24.728279'),
(5, 'admin', '0001_initial', '2023-02-08 05:39:24.882307'),
(6, 'admin', '0002_logentry_remove_auto_add', '2023-02-08 05:39:24.895540'),
(7, 'admin', '0003_logentry_add_action_flag_choices', '2023-02-08 05:39:24.903367'),
(8, 'contenttypes', '0002_remove_content_type_name', '2023-02-08 05:39:24.985957'),
(9, 'auth', '0002_alter_permission_name_max_length', '2023-02-08 05:39:25.073548'),
(10, 'auth', '0003_alter_user_email_max_length', '2023-02-08 05:39:25.110034'),
(11, 'auth', '0004_alter_user_username_opts', '2023-02-08 05:39:25.132449'),
(12, 'auth', '0005_alter_user_last_login_null', '2023-02-08 05:39:25.208093'),
(13, 'auth', '0006_require_contenttypes_0002', '2023-02-08 05:39:25.215140'),
(14, 'auth', '0007_alter_validators_add_error_messages', '2023-02-08 05:39:25.231203'),
(15, 'auth', '0008_alter_user_username_max_length', '2023-02-08 05:39:25.265460'),
(16, 'auth', '0009_alter_user_last_name_max_length', '2023-02-08 05:39:25.298495'),
(17, 'auth', '0010_alter_group_name_max_length', '2023-02-08 05:39:25.338165'),
(18, 'auth', '0011_update_proxy_permissions', '2023-02-08 05:39:25.364529'),
(19, 'auth', '0012_alter_user_first_name_max_length', '2023-02-08 05:39:25.403328'),
(20, 'sessions', '0001_initial', '2023-02-08 05:39:25.461423'),
(21, 'art', '0001_initial', '2023-02-08 08:48:11.888577'),
(22, 'art', '0002_alter_product_image', '2023-02-08 09:22:30.116171'),
(23, 'art', '0003_alter_product_image', '2023-02-08 16:19:32.248016'),
(24, 'art', '0004_wishlist', '2023-02-10 15:48:58.469550'),
(25, 'art', '0005_cart', '2023-02-14 14:57:45.705432'),
(26, 'art', '0006_contact', '2023-02-14 15:29:36.719575'),
(27, 'art', '0007_customer_delete_cart', '2023-02-15 15:39:00.578529'),
(28, 'art', '0008_delete_wishlist', '2023-02-16 15:02:19.144524'),
(29, 'art', '0009_order_orderitem', '2023-02-17 09:13:32.550070'),
(30, 'art', '0010_order_paid_order_payment_id', '2023-02-17 09:23:59.397286'),
(31, 'art', '0011_alter_order_phone', '2023-02-17 09:58:21.108379'),
(32, 'art', '0012_delete_customer', '2023-02-17 13:44:18.715984'),
(33, 'art', '0013_order_orderstatus_order_payment_method', '2023-02-18 12:50:28.551483'),
(34, 'art', '0014_rename_orderstatus_order_order_status', '2023-02-18 12:50:28.567559'),
(35, 'art', '0015_customer', '2023-02-19 03:51:46.573329'),
(36, 'art', '0016_wishlist', '2023-02-19 16:14:35.550243'),
(37, 'art', '0017_alter_order_amount', '2023-02-20 12:33:47.913701'),
(38, 'art', '0018_alter_order_phone', '2023-02-20 13:26:54.491098'),
(39, 'art', '0019_alter_order_phone', '2023-02-20 13:28:36.089597'),
(40, 'art', '0020_alter_order_amount', '2023-02-20 13:38:51.683818'),
(41, 'art', '0021_delete_customer', '2023-02-21 15:41:14.088997'),
(42, 'art', '0022_alter_order_firstname', '2023-02-21 15:45:41.846778'),
(43, 'art', '0023_alter_order_firstname', '2023-02-21 15:51:55.279270'),
(44, 'art', '0024_alter_orderitem_image', '2023-02-22 11:15:21.524517'),
(45, 'art', '0025_alter_orderitem_image', '2023-02-22 11:18:23.145305'),
(46, 'art', '0026_alter_order_amount', '2023-02-23 10:40:55.140532'),
(47, 'art', '0027_alter_order_amount', '2023-02-23 10:44:13.977147'),
(48, 'art', '0028_alter_orderitem_order', '2023-02-23 15:29:13.184200'),
(49, 'art', '0029_alter_order_date', '2023-02-23 15:58:21.879285'),
(50, 'art', '0030_alter_orderitem_product', '2023-02-24 11:33:55.494816'),
(51, 'art', '0031_alter_orderitem_product', '2023-02-24 11:33:56.244853'),
(52, 'art', '0032_alter_orderitem_product', '2023-02-24 11:33:56.369860'),
(53, 'art', '0033_orderitem_name', '2023-02-24 11:46:08.813195'),
(54, 'art', '0034_remove_orderitem_name_alter_orderitem_product', '2023-02-24 12:43:40.641793'),
(55, 'art', '0035_alter_orderitem_product', '2023-02-24 12:52:21.239139'),
(56, 'art', '0036_alter_order_payment_method', '2023-02-25 16:15:15.464242'),
(57, 'captcha', '0001_initial', '2023-02-26 11:22:59.903774'),
(58, 'captcha', '0002_alter_captchastore_id', '2023-02-26 11:22:59.903774'),
(59, 'art', '0037_remove_wishlist_slug', '2023-03-01 06:36:55.840303'),
(60, 'art', '0038_admin', '2023-03-13 12:13:25.245133'),
(61, 'art', '0039_alter_order_order_status_alter_orderitem_image_and_more', '2023-03-15 04:32:26.844682'),
(62, 'art', '0040_alter_orderitem_image', '2023-03-15 04:35:21.410104'),
(63, 'art', '0041_alter_orderitem_image', '2023-03-15 04:44:10.008085'),
(64, 'art', '0042_alter_order_order_status', '2023-03-15 06:44:13.586456'),
(65, 'art', '0043_alter_order_order_status', '2023-03-15 07:31:35.834456'),
(66, 'art', '0044_alter_order_order_status', '2023-03-17 09:45:28.770986'),
(67, 'art', '0045_alter_admin_user', '2023-03-17 14:02:46.822157'),
(68, 'art', '0046_contact_number', '2023-03-19 13:06:26.845240'),
(69, 'art', '0047_alter_order_order_status', '2023-03-20 08:00:09.598221'),
(70, 'art', '0048_product_average_rating_rating', '2023-03-20 11:51:40.260687'),
(71, 'art', '0049_remove_product_average_rating_delete_rating', '2023-03-20 14:17:19.946723');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('21kmpleno8jk94xf9hiyu7qmv5jliybi', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pSaju:ApDMv-UTFb4JBlVXQ6aGZBlzIDVR45usTM0-RWo7SUM', '2023-03-02 09:39:42.945430'),
('2aktq74b4w77mxos7mgxtsg1cf5d0rmc', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pc2qe:XLqFALhX9jEJQYEJH0N4Jpj0twpOM0JJb2z9_rEp8_8', '2023-03-28 11:29:44.564470'),
('2usjqkr898bnljjn6ji96tmwsclw1jhe', '.eJxVj0FuhDAMRe-SNQoJCWHCsvtKvQEysQuZloRCWIxG3H2SikW7sSz__5_tJxvgSPNw7LQNHlnPtGHV3-EI7otCUfAOYYrcxZA2P_Ji4Ze68_eI9P12ef8BZtjnnBY3Ba1shXGN0baTmkYiuimL6Mi4T-zQkjMtGtmMFg1K3dqma7UYm05ZlaEOtsT6J9OlFHY5WJuKrVvEw6XfB3TFAiyUN36AD8mHSebozwG5Tw_Wy2L3rhg6IQQXIst-galM6oXQQ73OMcW9vrB7DWtBKX5fJ3ae5ws-wWO8:1pc6hH:AlD4Ynh8AMe1NwLnnIgXp_bbgelRfRulP6YjoDGl6O0', '2023-03-28 15:36:19.455258'),
('3cg1rbotrutry9flsmq9dnab3jiny69k', '.eJxVjDsOwjAQBe_iGlmsE39CSc8ZovXuGgeQLcVJhbg7spQC2jcz761m3Lc8703WeWF1UVadfreI9JTSAT-w3KumWrZ1ibor-qBN3yrL63q4fwcZW-41k3EQDBoKQAZkhMTjGVFcTALsrTVTQO8Jk4k42WGQABN4HknEgfp8Afm9OFw:1pPnR6:UaOheoVLPm79RRrWUGb5nQWdzkrUXh3ojGDdtrP1VVo', '2023-02-22 16:36:44.384053'),
('3nj5i8yjzaj8xyf6162qip9t5nv3a3do', '.eJxVjDsKwzAQRO-iOgivVouklOlzBrH6RU6CDJZdmdw9NrhIqoF5b2YTntel-rXn2Y9JXAWIy28XOL5yO0B6cntMMk5tmccgD0WetMv7lPL7drp_B5V73ddOMxIyUcqgQrQmkClkBk6oYtkzq6yjsQjWOAKFrqAaIFqwBIBafL7SLDas:1pSB7A:pw1olDPkvAb7nljJR-QblnYsaXJICW9yxpKhQWh1VsM', '2023-03-01 06:18:00.539970'),
('4r97s546914ii7cl6w0nyixsog0pzcol', '.eJxVjDsOgzAQBe_iOrL8XWPK9JwB2bubQBKBZEOFuHtAokjaN_NmE31al6FfK5d-JNEKB-L2O-aEb55OQq80PWeJ87SUMctTkRetspuJP_fL_QsMqQ7HWzU2ee0VoAEXg3acmbmxkQgZ8EGBIiN4Am1yJCDtfDTBO5VNsNEeUUxlEe22719D1zsM:1pbhvZ:JokFv6-ln0B_ZqrxGOCQOzNRaiN4joA--e3c5nhp3Yw', '2023-03-27 13:09:25.902116'),
('5gzdktz1zfk2gkp3h7r3i4hu5o2jzs0a', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pc5lS:8vF6b1uJr-iK39ehvApFzh7kplajNQG3bbP8I0Vj22A', '2023-03-28 14:36:34.566010'),
('5wce7b7r242iswxtxewuopobkhldzdkd', '.eJxVjDsOgzAQBe_iOrL8XWPK9JwB2bubQBKBZEOFuHtAokjaN_NmE31al6FfK5d-JNEKB-L2O-aEb55OQq80PWeJ87SUMctTkRetspuJP_fL_QsMqQ7HWzU2ee0VoAEXg3acmbmxkQgZ8EGBIiN4Am1yJCDtfDTBO5VNsNEeUUxlEe22719D1zsM:1pedgQ:j02xzdu931qOVKbpCfB-T-ahA_ajtUzid0D4zfel1NU', '2023-04-04 15:13:54.146812'),
('6kfsgtqu6x7fdqpac0ap8osckz92x8sz', 'e30:1pPnCI:yA6OOc5x3lfk3oRWphu_F8MODEy8lB6yaA78TRf5g4I', '2023-02-22 16:21:26.955270'),
('6syx3htj49hgi589qb2ajpvrhseolr1y', '.eJxVjDsOgzAQBe_iOrL8XWPK9JwB2bubQBKBZEOFuHtAokjaN_NmE31al6FfK5d-JNEKB-L2O-aEb55OQq80PWeJ87SUMctTkRetspuJP_fL_QsMqQ7HWzU2ee0VoAEXg3acmbmxkQgZ8EGBIiN4Am1yJCDtfDTBO5VNsNEeUUxlEe22719D1zsM:1pWFC7:8hlHSLG2QS2mCy8ewtQuxBK37K_wJ-psHir-_twoYXA', '2023-03-12 11:27:55.622659'),
('78x5moxg1crcdydxj4l3mhbbyyzc15f3', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1paeBn:9BYPhlNehp3qtPWmjub-MuWbe20R1DoxEG5z8mEzFsw', '2023-03-24 14:57:47.102968'),
('8dl5uz7uxw04l68w3mka27r0ub6qsqfg', '.eJyVkF9vgyAUxb8Lzwb5I0P72Oy1yZJ9gOYKqLQVnWKWpel3H3TOrVlquhdI7rmc3zmc0R4m3-yn0Qx7q9EGUZT8npWgjsZFQR_A1R1WnfODLXFcwbM64l2nzWk7794YNDA24XWRARcchNCGslLlshSyEpKA5kxV4TbMZErmnOayEJTxouKMUJXTXFDKs2CqYPBoc0YsHtE75qUJ6odOT8pf47MEOWhNAD4P8G5dHeu8TeC89R9XuR-sinpGCCYkqLaFOg7S1mgLad90vhvT2XRMdfCh-NDX6JIgvoLmC_r1aLxq7pLlg-SRf2PFClYs2BewAeZqdgOmP-An8iAZ-mi1tJYreLngd-A0nOAunT9KL9svp1jfXANkKwGyP_3pvQTyf_3n779cPgGtpgER:1pRsyh:U5A9ddz1GN_pXnhlbXSgNfPNmApWFxwtfhtNbGCU7hU', '2023-02-28 10:56:03.909338'),
('8fwwflnk6rsgmnqisy665cwn1wxck4bv', '.eJxVjkFuxCAMRe_COiIYQiGz7L5SbxA5QBKmDaSBLEaj3H2gyqLd2Nb_389-kgGPvAxHcvvgLbkRIM1fbUTz5UI17B3DHKmJIe9-pDVCLzfRj2jd9_uV_QdYMC1lu-9QSIFSWgd8NFqNUk1SMbSCm6l0x11nlBagVS-Bi34SnIHRoCWA6ArU4J7J7UlkLZVd_4WGbHu0h8m_78uGBFxdOfiJPmQfZl42fw4sc35ccW9q4I0xRhkrtl9xrkq7Ouux3ZaYY2ovbGpxqyig920m53m-AB_KYm8:1pRwvk:FXXfwc2RRyMT_CBn604cLs0eUdlv3Lm52ecI2zr2waE', '2023-02-28 15:09:16.781189'),
('8kgbeyhhl7hxa7w6lpno178380xw11wl', '.eJxVjDsOwjAQBe_iGlmsE39CSc8ZovXuGgeQLcVJhbg7spQC2jcz761m3Lc8703WeWF1UVadfreI9JTSAT-w3KumWrZ1ibor-qBN3yrL63q4fwcZW-41k3EQDBoKQAZkhMTjGVFcTALsrTVTQO8Jk4k42WGQABN4HknEgfp8Afm9OFw:1pQULX:D6SoJ9YM2b952pfvlLWzE3b5o-yol19LQ40eShS4zzM', '2023-02-24 14:25:51.138339'),
('8lvjlag7n9mcqwqfmjnlbhbi7043m5m6', '.eJyVkMFuhCAURf-FtUEeSNFZdj_fYJ6AynQEK7hoJv57oXXRNplFN5C8e9_hhAfpcU9zv0e79c6QCwFS_ZwNqN-sL4G5oZ8C1cGnzQ20VOiZRnoNxt5fz-4vwIxxzttdg0IKlNJY4INu1SDVKBVDI7ge8225bbRqBbSqk8BFNwrOQLfQSgDRZKjGLZHLg6hyFHbxhYqsWzC7Tl_6qiIeF5sfvKI3eEeeF9939Mmlj7PtdMkFY4wylmO34FQm9WKNw3qdQwqxPqmxHpZvkqC31U7kqPIXPReAvwLwTODlvwJwChzHJ5tGjCQ:1pV9m0:a3i7oM0w0EDoz-hVmP6m7nOUzyF0bnznRM4c4CGkQOg', '2023-03-09 11:28:28.693178'),
('8t3y4ywro092b1tmfgj4zgfiu1bec6yo', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pSJnS:CSiEDfa0NEZgHmfTZ2tpu2k6R0TuI5a-5DpM_FgfAb8', '2023-03-01 15:34:14.971908'),
('90o63fzf0225wy54m9ky9elv4ttv1jch', '.eJxVj8lyhCAQht-Fs4WNIC7Huc8zWM0yymQEI3hITfnugcRDcunDv3zd_SYTHmmZjmj3yRkyEsFJ9VdUqD-sL455op8D1cGn3SlaIvRyI70HY1-3K_sPsGBcchtZM0hhueYNFwjSoFVMtI_GSoatVHowDw6iBeQ96KZVXT90SkKvckIzkaEa90TGN-nKKOxysOAV2fZgDp1-Hugq4nG1eeMdvcEXNrn5eaBPLn2RkZW008XnAEABsu1WnItSr9Y4rLclpBDrixprtf6SOH1udibneX4Dzo9kGA:1pTicZ:07IPazeo8KWg-2w6HrKWHLyX6ZNVY0GwLfddLk-eBMk', '2023-03-05 12:16:47.690437'),
('aqi3pqfo089v5c0mgi2s8rz63pu0utyr', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pTb89:qTjaaNAMKQ1Wq_6uTKm5jPkOeSmbZF2Ug7sqs8tgDv0', '2023-03-05 04:16:53.177168'),
('cito4ge4lpnac7yl8ebzaswkr25hkj1f', '.eJxVjDsKwzAQRO-iOgivVouklOlzBrH6RU6CDJZdmdw9NrhIqoF5b2YTntel-rXn2Y9JXAWIy28XOL5yO0B6cntMMk5tmccgD0WetMv7lPL7drp_B5V73ddOMxIyUcqgQrQmkClkBk6oYtkzq6yjsQjWOAKFrqAaIFqwBIBafL7SLDas:1pQWbB:G6q9csN-hKtHTPuXgadUdgcNqOB5qu2a4XET-z5fyM8', '2023-02-24 16:50:09.340769'),
('dwzbxln8sqcefj5vfl2l9btph7wxu404', '.eJxVjDsOgzAQBe_iOrL8XWPK9JwB2bubQBKBZEOFuHtAokjaN_NmE31al6FfK5d-JNEKB-L2O-aEb55OQq80PWeJ87SUMctTkRetspuJP_fL_QsMqQ7HWzU2ee0VoAEXg3acmbmxkQgZ8EGBIiN4Am1yJCDtfDTBO5VNsNEeUUxlEe22719D1zsM:1pebjB:G2UhlzSaMtCG4bB3fxZlUqpgEi5Cd5EUJMEUmhefHrs', '2023-04-04 13:08:37.030997'),
('e9c450jec220inra5bhqdmaox7cnymcc', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pcLTH:JUkS0hsaiWW-WYD4iq_TuX9Yn1uoIiIv70wTh4F7w24', '2023-03-29 07:22:51.208950'),
('fbppwo3p7s3urq9tyw3qvc7ij7jxcg43', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pblhT:yDZ6m2WbpqkWQ3HiagoReUS6WAPe3vkq8oTUcwHOG34', '2023-03-27 17:11:07.124657'),
('grutf9xr44vdt6bhqiz4xft8qm4g0zvl', '.eJxVjs2SgyAQhN-Fs4X8SIE55p5nsIYflSSCETykUr57YNfD7mWmarr7m_6gAfY8D3ty2-AtuiCKmr83DebhQhXsHcIUsYkhb17jasGnmvAtWve8nt5_gBnSXNJ9B1xwEMI6yrRRUgs5CknAcmbGsh1znZGKUyV7QRnvR84INYoqQSnvCtTAltHlg2QdlV370gatW7S7yT_1ZYMCLK48vEGw8ARWgq8dQvb5fbq9qTonhGBCiuwXmOqlXZz10K5zzDG1JzW1evklcXxf3YSO4_gC06Zi_g:1pTMBB:UsEwCt5k90T54lk4D258eOgBu6fquVzs0iEEs-ncsPg', '2023-03-04 12:19:01.871496'),
('hdlift76lpamvdav7ea0c00yv9j25czh', '.eJyNj8lOxDAQRP-lz5HjLdscR1znG6L2MomHiR0SRwiN8u_YECE4gLj0oav6VfUDetzi2G-rXXpn4ARSQPF9qVA_W58Vc0M_BKKDj4tTJFvIoa7kEoy9nw_vD8CI65iukfGullZowYVEWhu0isnqym3NsKqV7sxVUFlRFC3VvFJN2zWqpq1KDs1kgmpcIpwe0OSR2bmwFAXMSzCbjh8PNAV4nGxKvKA3eEeeLl829NHFNzix7HY664JSSihNsptwyJtyssZhOY8hhrU8qGuppk-SILfZDrAXwP9qwL8aPC346vzAfmsg_1nAJA5L4Sl7398BaqmLew:1pU3qT:QHaC5dimzLijobw4C_nTsrxwzie_YelO5rdGMcom2iA', '2023-03-06 10:56:33.346519'),
('i932e6vhzds5bttmn3t2hvrasnc5570a', '.eJxVjDsKwzAQRO-iOgivVouklOlzBrH6RU6CDJZdmdw9NrhIqoF5b2YTntel-rXn2Y9JXAWIy28XOL5yO0B6cntMMk5tmccgD0WetMv7lPL7drp_B5V73ddOMxIyUcqgQrQmkClkBk6oYtkzq6yjsQjWOAKFrqAaIFqwBIBafL7SLDas:1pPgBr:D-3kwvscnHa1eaN4qo19inBOiYioA1y91P4Dhw3Npdc', '2023-02-22 08:52:31.699163'),
('ij075h6gw8bd8hbssx8ao4tdg6383xsl', '.eJxVjkFuxCAMRe_COiIYQiGz7L5SbxA5QBKmDaSBLEaj3H2gyqLd2Nb_389-kgGPvAxHcvvgLbkRIM1fbUTz5UI17B3DHKmJIe9-pDVCLzfRj2jd9_uV_QdYMC1lu-9QSIFSWgd8NFqNUk1SMbSCm6l0x11nlBagVS-Bi34SnIHRoCWA6ArU4J7J7UlkLZVd_4WGbHu0h8m_78uGBFxdOfiJPmQfZl42fw4sc35ccW9q4I0xRhkrtl9xrkq7Ouux3ZaYY2ovbGpxqyig920m53m-AB_KYm8:1pS7eg:TAJ5a3Unt42_uuXjSWAOPffEy7iS67QjBS7lt0NCYtc', '2023-03-01 02:36:22.149058'),
('ikr4eyf5m250yl0n3giuqgb6rain3b1o', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pc4HB:qGG1aakeCjNTtnraENlv7_OBFaqUDOjBri5vq9rfSoU', '2023-03-28 13:01:13.787640'),
('ixko6svwdktzet6nhpyoqot386uv2djp', '.eJxVjDsKwzAQRO-iOgivVouklOlzBrH6RU6CDJZdmdw9NrhIqoF5b2YTntel-rXn2Y9JXAWIy28XOL5yO0B6cntMMk5tmccgD0WetMv7lPL7drp_B5V73ddOMxIyUcqgQrQmkClkBk6oYtkzq6yjsQjWOAKFrqAaIFqwBIBafL7SLDas:1pPgEL:ExD0TSvYyL0Toup_DILxEPLRiBTsr-uEaHbaa7X2UVc', '2023-02-22 08:55:05.597137'),
('j7bsaxw5axlo6ywc1i3fq1bq5dguumji', '.eJxVjDsKwzAQRO-iOgivVouklOlzBrH6RU6CDJZdmdw9NrhIqoF5b2YTntel-rXn2Y9JXAWIy28XOL5yO0B6cntMMk5tmccgD0WetMv7lPL7drp_B5V73ddOMxIyUcqgQrQmkClkBk6oYtkzq6yjsQjWOAKFrqAaIFqwBIBafL7SLDas:1pPeOm:K2-vru7juCxTmZA-9QKeUfmybLQNBklnkexqtOOOTqE', '2023-02-22 06:57:44.537369'),
('j8tdg6ndjz7os0q1y6nic7x7063tfygi', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pcJHK:ZmW2lvUNZZ-P5q5j2e6Zyju-iNoFuLRzu4Ov25gE3oo', '2023-03-29 05:02:22.508467'),
('je2lits4sjnvrep9n24t4mald25idjva', '.eJylj81ugzAQhN_FZwTG2JjkGPXKA_SE1n_gNNgUjKoo4t1rt7RqVTWq1MsedmZnvr2hDtYwdOui584qdESUoezrUoB80i4p6gyu97n0LsxW5MmS7-qSt17py2n3fgsYYBnitamEqg-1YFQbUWMlKeXaKA6EcU2owUBqzpjRglSU100jBRdENYabAzZSxlAJc0DHGyJppOwETFmGptmrVYa3B0iGHIw6Nj7M8GJdX8bL5xVcsOGKjmVyW5l0inGOcVTtCH1aFKNWFopp8MEvxR66FCrmlPl56tGWofJed_nZ3YJTcIFfu2v8x3Ix7kkRQL8T3Ef4wVD99_8PhKrztH3Up-uOsm2vwbm5IQ:1paEkp:8t5Z0L0C9G6IXl6cPW-1obmMLik1pSFC9SGF6D_CEOA', '2023-03-23 11:48:15.020951'),
('jkeprrkbo0itaiy4a0sb9mord234i87y', '.eJxVjDsOwyAQBe9CHSHwevmkTO8zoIWF2ElkJGNXUe4eIblI2jcz7y0CHfscjpa3sLC4Ci0uv1uk9MxrB_yg9V5lquu-LVF2RZ60yalyft1O9-9gpjb32ibURNllLKSMZw8OCAswW6sAjB4HHw16qzxHV9B6NDBCGYpJGbX4fAHiRjdi:1pPdZq:4YDnnxmsCjEoqBKeBg_9UtPVYvUY4c-qyTD8RVSmrpE', '2023-02-22 06:05:06.868277'),
('jkq5iicmmz81a1e76ngba0joj9koktqo', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pbczm:jUzhxJ9uaDfuAgsXpm-jGrc6jnNjsWjHj5gH7d4kITE', '2023-03-27 07:53:26.874737'),
('jm7p3ty0la3qymh3o20sb5jy8fpm63p1', '.eJyNj81OxCAUhd-FdUP5KdLO0ridZ2hugbaMU6hAY8yk7y4kxOhC4wbCPYfvnPtAIxxpHY9owmg1uiCKmu-zCdSrcUXQN3CLx8q7FOyEiwVXNeKr1-b-XL0_ACvENf8eOuCCgxDaUDapXk5CzkIS0JypOd-GmU7JntNeDoIyPsycEap62gtKeZehCkJCl0dumI_CLn1pg_bg9aHSWJ8ONpMDr-A03KGs83aASzZ9VLdVRX8ihGBCsmw3WMqk3Yy20O6rTz62lRrbaaskfNvNgs4GsT8KsK8CLwHerVt-LdD9M19nTsnO0ef5CV5tih8:1pRsgg:UeyjbPWwJPpuevrHTdyGLlyG1YKg4AjQ0eBGKinLs_4', '2023-02-28 10:37:26.999744'),
('jt6nwwj4t5bz92gq5vz9fdpizh1x2zkw', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pX2F0:5qqdIHXvIdpa5_vQTnnKYVCypPftVWGaeo8EOGTHbJ8', '2023-03-14 15:50:10.424585'),
('m9tzl0rvez2swgw78126kivbohih8f1u', '.eJyVks2OgyAURt-FtVEQLLTLZrZNJpnVrMwVqNJWcBQzaZq--0jrmPmJpt1gwr1853jhgnLofZX3nW5zo9AGERT93CtAHrUNBXUAW7pYOutbU8ShJR6rXbxzSp-2Y--vgAq6aji9ZkAzClmmNEkLKXiR8X3GMSiayv3w1almkgtKBF9nJKXrPU0xkYKIjBDKhlAJrUebCxJhCdnBl0SoaZ3qpb_piwhZqPUAfGnh09iSDgc_erDe-PPYbWSorzCOMR6qpoYybCS1VgaSpnLedckY2iXqnsPiQ1Oia4ToAp1O9Lej9rIic3D-ILyj31i-gOUTdgdWwQnSOS7FD4KL-p4U-PomQBYEyF-B2R9fPStAJgG2IMAmgVcwA9WWS6N_zACaEDVdAFkcwL8JzL479uwN5I7t3vX2PA7iev0CGgAvBQ:1pSthy:8Q_z53yLLWvA9_x3kocOHIVPb8-QUNeRLnJ8JMa8jTU', '2023-03-03 05:54:58.098798'),
('noolqbqav2vjmwhdie96r58kogfp4xxz', '.eJyNj81ugzAQhN_FZ2RYY36PUa-R-gZosQ04DTYFo6qKePfaKUr6I0W5WNbO7DezF9Lg6oZmXdTcaElqkpHo56xF8aZMEOQJTW-psMbNuqXBQnd1oUcr1fmwe38BBlyGsC0Fy6FkyEQJgoHi0EmeIKq87RTIIstYVWJRCOxYi1WWpqqECgrJhVI5eKjA2ZH6Qnh4Ajv0zSIyzVauwl3r84gYHJUPfEVtnDZ92Hxf0f_dJ6kh2LUIhiJJEpokXtYj9mESj0pqjKfBOrvEO3aJcQqolJ6mnmwRgQfxcIs_opF4xj_p-T0-fza-HXeUL6CuDdiDBuzW4GXGj3_3s3sB_mS-9Bz4Pn7bvgAW8LS1:1pQitb:mOFskACpyNkyX9lBUhjd234ClON88qBVJ62avVr8EJ8', '2023-02-25 05:57:59.144852'),
('od9zt9zykqa1yqq2nrsq76wz59ga75wr', '.eJxVjLEOgzAMRP8lcxUFiEnM2L3fgGxsGtoKJAIT4t8LEkM7nXT37m2mpXVJ7Zp1bgcxjamduf2WTN1bx3ORF43PyXbTuMwD2xOx15rtYxL93C_2T5Aop-MNUakOHCvwDrxHKODI3qtIXzspuQdwVSAGQCwIgVUIKQRCH0t_SjuaF9Ns-_4FI-I61g:1pfvrc:VVnBA8QLXh2XTh0d9r2u6kIkk1PdeUnqqGzkpl4IMrU', '2023-04-08 04:50:48.336889'),
('puo7wi30uz467rtxetg77ealy1g3fks6', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pPj46:yX08mmBNpAe4Si8WkrN5hr2OQeMOV3fkGwj1Uu7qrqM', '2023-02-22 11:56:42.112812'),
('q6xqot43bidlaz4ajsrj5c4a4yrmo72t', '.eJxVj0FuhDAMRe-S9SgkEMKEZdVtz4BM7EKmJaEhaDQacfcmLYt2Y8n-_z_bTzbAnuZh3ygODlnPlGaXv8MR7Af5ouAN_BS4DT5FN_Ji4ae68beA9Plyev8BZtjmnBbXBlrZCm1rrUwnFY1EdG0MoiVt37FDQ1a3qGU9GtQoVWvqrlVirLvGNBlqISbWP5kupbDLwUpf2BoD7jb9PJBbDwvlja8R7s5PdU5-7eCTSw_Wy-J2tuhKCC5EVt0CUxlUC6GDap1DClt1QrcKfzmS39aJHcfxDd0tY40:1pc3WK:SD3l3H652JKm0kznPYXbEK1mXYaaPtA5u2KfKBkPvs4', '2023-03-28 12:12:48.816347'),
('rjruukaqh5ecyr81itfx4egzjilcyzlp', '.eJxVjDsKwzAQRO-iOgivVouklOlzBrH6RU6CDJZdmdw9NrhIqoF5b2YTntel-rXn2Y9JXAWIy28XOL5yO0B6cntMMk5tmccgD0WetMv7lPL7drp_B5V73ddOMxIyUcqgQrQmkClkBk6oYtkzq6yjsQjWOAKFrqAaIFqwBIBafL7SLDas:1pPgUC:5nsHiS2qxkl-oG8LvzDtGdDLQNRuD5cZuNqXsCZ3LY4', '2023-02-22 09:11:28.668702'),
('rmb3x5dteohxlzccp97hgt0dhhakt1rq', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pcKuR:OQtM_NxsEliCW9MohNW9GdDpkWaYAYPO4jaS6TgKlJ4', '2023-03-29 06:46:51.287665'),
('rp5qfja7mx1neta06891jwb0kamiy0ni', '.eJyNjstOhDAUht-la1LaAm1hady68gHI6QXozNAiLTFmwrtLlRhdaNycxfkv339HPWxp6rdo194Z1CGBiu8_BfpqfRbMBfwYsA4-rU7hbMGnGvFTMPb2cHp_FEwQpyPNK0ob4I0aFPBBcVNLNbSNZM0ATBkCTAqhpJQtaxVVLddS1KTm2hJhoM6rNKwJdXfE8sndea8o0LIGs-n0MZ8VyMNsD-DjCq_Oj_QIvmzgk0tvqKPZ7XTWa0IwIYfqZhjzo5ytcVAuU0ghlmdpLM3RQ_FlGdFeoOoPdPWFfr7apKdfyeKf5Fh9Yvf9HTUnh6Q:1pSH1w:8orhenQCdBjZQtkqIjCrdMG7iFp0M2iOmXWRBMU5Qu8', '2023-03-01 12:37:00.876300'),
('sjrtfawjakm4nr8qsjgcaq2u2zonw3a4', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pPiW4:or2YjFLOlFoevl4iCsuo19bU1a8lxUtQuVqwzm7GGyY', '2023-02-22 11:21:32.637860'),
('t4pxbsxgna0bg4uv910x7fsyf6qlggnp', '.eJxVjDsKwzAQRO-iOgivVouklOlzBrH6RU6CDJZdmdw9NrhIqoF5b2YTntel-rXn2Y9JXAWIy28XOL5yO0B6cntMMk5tmccgD0WetMv7lPL7drp_B5V73ddOMxIyUcqgQrQmkClkBk6oYtkzq6yjsQjWOAKFrqAaIFqwBIBafL7SLDas:1pPg1M:_PqH51eYaO_PJIz6pyM21w1TpKP2_Pl2HVmghL7JuvA', '2023-02-22 08:41:40.106870'),
('thugj6e1d8uvyi156xlybi4mdam1tcpa', '.eJxVj8uOwyAMRf-FdUV4hEK67L7fEJlHEjoNpIEsRlX-fUBi0dnYsu_1sf1BIxx5GY_k9tFbdEMUXb57GsyPC1WwTwhzxCaGvHuNqwU3NeFHtO51b95_gAXSUqaHHrjgIIR1lGmjpBZyEpKA5cxMJTvmeiMVp0oOgjI-TJwRahRVglLeF6iBPaPbp1xYQmXXe-kFbXu0h8ljKwOsrix8QLDwgvrO-4CQff5tbm-qfiWEYEKK7FeYa6dbnfXQbUvMMXWNmjq9NhJ-bm5G53n-Ac6LYvI:1pPj5U:t8QDvDNFcohpW1LbCwQP_lF3M-uJXqAMrs4h6STNuFs', '2023-02-22 11:58:08.203401'),
('um11uem44ouyb1onqblmzfs4ssht12df', '.eJyVj0FuwyAQRe_C2sIMOEbOsvucwZoBYpPG4Nh4UUW5eyG11KaLSt2wmP_n8ebOetzS2G-rW3pv2ZHJjlU_h4Tm3YWS2AuGIXITQ1o88VLhe7ryU7Tu-rZ3XwAjrmPeJlJCggF9INUBKHNGoTWCaDoQVjogg7a1utGqIaOkNGeiQ662bUNWNBlqcEnseGe6PIVdhGVXsXmJdjPpeYCuWMDJ5R9PGCxeUebN24Yh-fTxjOfFm5IrIQQXIsd-wqFM6slZj_U8xhTXeqeuNU1fJMUvsxvYo2LwlwH8NoAXA_lt0P7XAHaDx-MTyGyNnQ:1pTQho:jzJGVxD834GrGeSbe4-BeOOjhqQZWytrlFQwMvpd6PM', '2023-03-04 17:09:00.525487'),
('wcu7yjzmv6mfpriiqva30ixjj2a1higq', '.eJyNj81uhCAURt-FtUF-JKjL7idp0gcwV0BlZgSruGgmvnuhNaYuOpkNJPe7nPPxQA2sYWjWxcyN1ahGFGV_Zy2om3Ep0FdwvcfKuzDbFqcVvKcLvnht7m_77gkwwDLE11UBXHAQQhvKWlXKVshOSAKaM9XF2zBTKFlyWspKUMarjjNCVUlLQSkvIlTBHFD9QDwdiZ360gxNs9erCj_1eYYcjCYKP24mqCH95nMFF2z4QnWRlq1KsSQEExJTO0KfBvlotIV8GnzwS74zl3zh-Dr1aMtQ8URbHNp3sFHm-rOYncSvmWFKqEMvn-jlob-A03AH9p-dv2pvx19S8ptYYNu-AXrMsTU:1pSLp6:lcljaG9xs3WXJEzXKGOMwAhIWe4jXF5RZ8I1EA2ILf0', '2023-03-01 17:44:04.340504'),
('wqqonrx6iwil5pcobqcub05muefyyq9m', '.eJyNj81uhCAUhd-FtUF-pKLLSbfzDObyozIdwQqmaSa-eyExbWfTdAPhnsN3zn2gAfY0D3u02-AM6hFF1e-ZAv1mfRHMDfwUsA4-bU7hYsGnGvE1GHu_nN4nwAxxzr-7BrjgIISxlCktWyXaUbQEDGd6zLdlttGt5FS2naCMdyNnhGpJpaCUNxmqYUuof-SG-Sjs0pdWaN2C2XUazqeHxebAK3gDdyjrvO_gk0ufqJfF7XTRXwghmJAsuwWmMqkXaxzU6xxSiPVJjbVaThK-rXZCR4XYHwXYd4HXDT6cn54L0J8CzT_zTeaU7Bx9HF9jtIom:1pRnbS:u81A-7I30Cs9tFuSuAW-FnGFJbGoKJCdgcDuvHY88ec', '2023-02-28 05:11:42.767943'),
('xr144eewx8wdaz2sfv77kvkplwpdy58t', '.eJxVjDsOgzAQBe_iOrL8XWPK9JwB2bubQBKBZEOFuHtAokjaN_NmE31al6FfK5d-JNEKB-L2O-aEb55OQq80PWeJ87SUMctTkRetspuJP_fL_QsMqQ7HWzU2ee0VoAEXg3acmbmxkQgZ8EGBIiN4Am1yJCDtfDTBO5VNsNEeUUxlEe22719D1zsM:1pdrM2:TcEYotX1BbfX6ArHn-vq91op8RUwUPrgvX3n1j5KfAA', '2023-04-02 11:37:38.916617'),
('xyhqas7qummxsphuyuujfsipyb1xyw9v', '.eJxVj81uhCAQx9-Fs0FAxMXjptc-gxmZqbKtYBXTNBvfvdB4aC9z-H_8ZubJBjjSPBw7bYNH1jNtWPVXHMG9UygOPiBMkbsY0uZHXiL8cnf-GpE-7lf2H2CGfc5tcWugla0wThltO6lpJKJbYxEdGfeGHVpypkUj1WjRoNStVV2rxai6xjYZ6mBLrH8yVUZhl4O1qdi6RTxc-n1AVSzAQnnjywZfPkwyNz8PCMmnb9Y3Je1d8bUQXIjs-gWmItQLoYd6nWOKe31B9xozR_LHOrHzPH8ArvZiSA:1paeeq:ObqmWRgzGUVX5nDFc_7oWgCCmS68vK0cv-B4paMK-CM', '2023-03-24 15:27:48.070316'),
('yf59syn6wa9tj7xohy97v0xd1jfe2lri', '.eJxVjDsOgzAQBe_iOrL8XWPK9JwB2bubQBKBZEOFuHtAokjaN_NmE31al6FfK5d-JNEKB-L2O-aEb55OQq80PWeJ87SUMctTkRetspuJP_fL_QsMqQ7HWzU2ee0VoAEXg3acmbmxkQgZ8EGBIiN4Am1yJCDtfDTBO5VNsNEeUUxlEe22719D1zsM:1pc6F5:F5R76A2mZZI2GVEqbI_B92hvozLtBbnVeeKxYaFZCZY', '2023-03-28 15:07:11.435510'),
('zfojs3z0bpvx1g5kocxkmtwhd1rsipnx', '.eJxVjMEKgzAQRP8l5xLcrEuix977DbLZxGpbEjB6Ev-9ETy0p4GZN29XA2_rNGwlLsMcVK9A3X47z_KO6RzCi9Mza8lpXWavT0Rfa9GPHOLnfrF_gonLVN9dy0jIRCGC8eKsJzuSbTigkbFmNLEV6xCc7QgMdiOaBsSBIwBsq1R4WVW_H8cXA705-A:1pRus6:HrjkDOY6TCeCr9Eo8IjVWhMaf0Qva2EOac3o82pg7e4', '2023-02-28 12:57:22.633099');

-- --------------------------------------------------------

--
-- Table structure for table `projectapp_customer`
--

CREATE TABLE `projectapp_customer` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projectapp_customerorder`
--

CREATE TABLE `projectapp_customerorder` (
  `id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `ordered_date` datetime(6) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shop_category`
--

CREATE TABLE `shop_category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shop_product`
--

CREATE TABLE `shop_product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `description` longtext NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `available` tinyint(1) NOT NULL,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `category_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `art_admin`
--
ALTER TABLE `art_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `art_category`
--
ALTER TABLE `art_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `art_contact`
--
ALTER TABLE `art_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `art_order`
--
ALTER TABLE `art_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `art_order_user_id_f15dabfa_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `art_orderitem`
--
ALTER TABLE `art_orderitem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `art_orderitem_order_id_8d5126df_fk_art_order_id` (`order_id`);

--
-- Indexes for table `art_product`
--
ALTER TABLE `art_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `art_product_category_id_f71c277a_fk_art_category_id` (`category_id`);

--
-- Indexes for table `art_wishlist`
--
ALTER TABLE `art_wishlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `art_wishlist_product_id_9f0d5ff2_fk_art_product_id` (`product_id`),
  ADD KEY `art_wishlist_user_id_a216dd77_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `captcha_captchastore`
--
ALTER TABLE `captcha_captchastore`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hashkey` (`hashkey`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `projectapp_customer`
--
ALTER TABLE `projectapp_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projectapp_customerorder`
--
ALTER TABLE `projectapp_customerorder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Projectapp_customero_customer_id_6aaaa67b_fk_Projectap` (`customer_id`),
  ADD KEY `Projectapp_customerorder_product_id_09299bf3_fk_shop_product_id` (`product_id`);

--
-- Indexes for table `shop_category`
--
ALTER TABLE `shop_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `shop_category_name_11b68823` (`name`);

--
-- Indexes for table `shop_product`
--
ALTER TABLE `shop_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_product_category_id_14d7eea8_fk_shop_category_id` (`category_id`),
  ADD KEY `shop_product_name_b8d5e94c` (`name`),
  ADD KEY `shop_product_slug_30bd2d5d` (`slug`),
  ADD KEY `shop_product_id_slug_68aad32e_idx` (`id`,`slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `art_admin`
--
ALTER TABLE `art_admin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `art_category`
--
ALTER TABLE `art_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `art_contact`
--
ALTER TABLE `art_contact`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `art_order`
--
ALTER TABLE `art_order`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=262;

--
-- AUTO_INCREMENT for table `art_orderitem`
--
ALTER TABLE `art_orderitem`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;

--
-- AUTO_INCREMENT for table `art_product`
--
ALTER TABLE `art_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `art_wishlist`
--
ALTER TABLE `art_wishlist`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `captcha_captchastore`
--
ALTER TABLE `captcha_captchastore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=311;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `projectapp_customer`
--
ALTER TABLE `projectapp_customer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projectapp_customerorder`
--
ALTER TABLE `projectapp_customerorder`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shop_category`
--
ALTER TABLE `shop_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shop_product`
--
ALTER TABLE `shop_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `art_admin`
--
ALTER TABLE `art_admin`
  ADD CONSTRAINT `art_admin_user_id_195e58c7_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `art_order`
--
ALTER TABLE `art_order`
  ADD CONSTRAINT `art_order_user_id_f15dabfa_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `art_orderitem`
--
ALTER TABLE `art_orderitem`
  ADD CONSTRAINT `art_orderitem_order_id_8d5126df_fk_art_order_id` FOREIGN KEY (`order_id`) REFERENCES `art_order` (`id`);

--
-- Constraints for table `art_product`
--
ALTER TABLE `art_product`
  ADD CONSTRAINT `art_product_category_id_f71c277a_fk_art_category_id` FOREIGN KEY (`category_id`) REFERENCES `art_category` (`id`);

--
-- Constraints for table `art_wishlist`
--
ALTER TABLE `art_wishlist`
  ADD CONSTRAINT `art_wishlist_product_id_9f0d5ff2_fk_art_product_id` FOREIGN KEY (`product_id`) REFERENCES `art_product` (`id`),
  ADD CONSTRAINT `art_wishlist_user_id_a216dd77_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `projectapp_customerorder`
--
ALTER TABLE `projectapp_customerorder`
  ADD CONSTRAINT `Projectapp_customero_customer_id_6aaaa67b_fk_Projectap` FOREIGN KEY (`customer_id`) REFERENCES `projectapp_customer` (`id`),
  ADD CONSTRAINT `Projectapp_customerorder_product_id_09299bf3_fk_shop_product_id` FOREIGN KEY (`product_id`) REFERENCES `shop_product` (`id`);

--
-- Constraints for table `shop_product`
--
ALTER TABLE `shop_product`
  ADD CONSTRAINT `shop_product_category_id_14d7eea8_fk_shop_category_id` FOREIGN KEY (`category_id`) REFERENCES `shop_category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
